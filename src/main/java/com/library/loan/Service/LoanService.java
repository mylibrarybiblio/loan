package com.library.loan.Service;

import com.library.loan.Bean.BookBean;
import com.library.loan.Bean.UserBean;
import com.library.loan.Dto.LoanDto;
import com.library.loan.Dto.ResponseLoanDto;
import com.library.loan.Entity.Loan;
import com.library.loan.Entity.LoanStatus;
import com.library.loan.Exception.LoanException;
import com.library.loan.Proxie.BookProxy;
import com.library.loan.Proxie.UserProxy;
import com.library.loan.Repository.LoanRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LoanService {
    @Autowired
    private LoanRepository loanRepository;
    @Autowired
    private BookProxy bookProxy;
    @Autowired
    private UserProxy userProxy;

    public void createNewLoan(LoanDto loanDto) {
        List<Loan> userHavenOpenForBook = loanRepository
                    .findByIdUserAndLoanStatus(loanDto.getIdUser(), LoanStatus.OPEN);
            if (!userHavenOpenForBook.isEmpty()){
                for (Loan l : userHavenOpenForBook){
                    for (Integer i : loanDto.getIdBook()){
                        if (l.getIdBook().contains(i)){
                            BookBean oneBook = bookProxy.oneBook(i);
                            throw new LoanException("Vous louez déjà le livre "+oneBook.getTitle()+"");
                        }
                    }

                }
            }
            for (Integer i : loanDto.getIdBook()){
                List<BookBean> verifBook = new ArrayList<>();
                verifBook.add(bookProxy.oneBook(i));
                if (verifBook.isEmpty()) throw new LoanException("Ce Livre n'exite pas");
            }
            Loan loan = convertToEntity(loanDto);
            loan.setLoanStatus(LoanStatus.OPEN);
            loanRepository.save(loan);
    }

    public List<LoanDto> loanDtoList(){
        List<Loan> loanList =  loanRepository.findAll();
        List<LoanDto> loanDtoList = loanList.stream().map(this::convertToDto).collect(Collectors.toList());
        return loanDtoList;
    }

    public List<ResponseLoanDto> searchLoanByEmail(String email) {
        List<UserBean> userBeanList = userProxy.SearchUser(email);
        List<BookBean> bookBeanList = new ArrayList<>();
        ResponseLoanDto responseLoanDto = new ResponseLoanDto();
        List<BookBean> listBookBean = new ArrayList<>();
        List<Loan> loanList = new ArrayList<>();

        if (userBeanList.isEmpty()) return null;
        for (UserBean userBean: userBeanList) {
            loanList = loanRepository.findByIdUser(userBean.getIdUser());
        }
            List<Integer> listIdBook = loanList.stream().flatMap(x->x.getIdBook().stream()).collect(Collectors.toList());
            for(Integer i :listIdBook){
                bookBeanList.add(bookProxy.oneBook(i));
            }
            List<LoanDto> loanDtoList = loanList.stream().map(this::convertToDto).collect(Collectors.toList());
            List<ResponseLoanDto> responseLoanDtoList = loanDtoList.stream().map(this::loanToRespLoanDto).collect(Collectors.toList());

        return responseLoanDtoList;
    }

    public List<ResponseLoanDto> searchLoansBeforeThisDate(String date) {
        SimpleDateFormat format =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<Loan> loanList = new ArrayList<>();
        List<BookBean> bookBeanList = new ArrayList<>();
        try {
            loanList=loanRepository.findByLoanBeginDateBefore(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Integer> listIdBook = loanList.stream().flatMap(x->x.getIdBook().stream()).collect(Collectors.toList());
        for(Integer i :listIdBook){
            bookBeanList.add(bookProxy.oneBook(i));
        }
        List<Integer> idUserList = loanList.stream().map(Loan::getIdUser).collect(Collectors.toList());
        List<UserBean> userBeanList = new ArrayList<>();
        for (Integer i : idUserList){
            userBeanList.add(userProxy.oneUser(i));
        }
        List<LoanDto> loanDtoList = loanList.stream().map(this::convertToDto).collect(Collectors.toList());
        List<ResponseLoanDto> responseLoanDtoList = loanDtoList.stream().map(this::loanToRespLoanDto).collect(Collectors.toList());
        return responseLoanDtoList;
    }

    private Loan convertToEntity(LoanDto loanDto){
        ModelMapper mapper = new ModelMapper();
        Loan loan = mapper.map(loanDto,Loan.class);
        return loan;
    }

    private LoanDto convertToDto(Loan loan) {
        ModelMapper mapper = new ModelMapper();
        LoanDto loanDto = mapper.map(loan, LoanDto.class);
        return loanDto;
    }


    public LoanDto updateLoan(LoanDto loanDto) {
        Loan loan = loanRepository.findById(loanDto.getIdLoan())
                .orElseThrow(()-> new LoanException("Ce prêt n'existe pas"));
        Loan loan1 = convertToEntity(loanDto);
        Loan updateLoan = loanRepository.save(loan1);
        LoanDto updateLoanDto = convertToDto(updateLoan);
        return updateLoanDto;
    }

    public void deleteLoan(int idLoan) {
        Loan loan = loanRepository.findById(idLoan)
                .orElseThrow(()-> new LoanException("Ce prêt n'existe pas"));
        loanRepository.delete(loan);
    }

    private ResponseLoanDto loanToRespLoanDto(LoanDto loanDto){
        if (loanDto == null){
            return null;
        }
        ResponseLoanDto responseLoanDto = new ResponseLoanDto();
        if (!loanDto.getIdBook().isEmpty()){
            List<BookBean> bookBeanList = new ArrayList<>();
            for (int i: loanDto.getIdBook()){
                if (loanDto.getIdBook().size()==1){
                    bookBeanList.add(bookProxy.oneBook(loanDto.getIdBook().get(0)));
                } else {
                    bookBeanList.add(bookProxy.oneBook(loanDto.getIdBook().get(i-1)));
                }
                responseLoanDto.setBookList(bookBeanList);
            }
        }
        responseLoanDto.setLoanUser(userProxy.oneUser(loanDto.getIdUser()));
        responseLoanDto.setLoanEndDate(loanDto.getLoanEndDate());
        responseLoanDto.setLoanBeginDate(loanDto.getLoanBeginDate());
        return responseLoanDto;
    }
}
