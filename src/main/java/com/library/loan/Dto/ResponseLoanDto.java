package com.library.loan.Dto;

import com.library.loan.Bean.BookBean;
import com.library.loan.Bean.UserBean;

import java.util.Date;
import java.util.List;


public class ResponseLoanDto {
    //private List<LoanDto> loanList;
    private Date loanBeginDate ;
    private Date loanEndDate ;
    private UserBean loanUser;
    private List<BookBean> loanBookList;

    public ResponseLoanDto() {
    }

    public ResponseLoanDto(Date loanBeginDate, Date loanEndDate, UserBean loanUser, List<BookBean> loanBookList) {
        this.loanBeginDate = loanBeginDate;
        this.loanEndDate = loanEndDate;
        this.loanUser = loanUser;
        this.loanBookList = loanBookList;
    }

    public Date getLoanBeginDate() {
        return loanBeginDate;
    }

    public void setLoanBeginDate(Date loanBeginDate) {
        this.loanBeginDate = loanBeginDate;
    }

    public Date getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(Date loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public UserBean getLoanUser() {
        return loanUser;
    }

    public void setLoanUser(UserBean loanUser) {
        this.loanUser = loanUser;
    }

 /*   public ResponseLoanDto(List<LoanDto> loanDtoList, List<UserBean> userList, List<BookBean> bookList) {
        this.loanList = loanDtoList;
        this.userList = userList;
        this.bookList = bookList;
    }

    public List<LoanDto> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanDto> loanDtoList) {
        this.loanList = loanDtoList;
    }*/

/*
    public List<UserBean> getUser() {
        return userList;
    }

    public void setUser(List<UserBean> userList) {
        this.userList = userList;
    }*/

    public List<BookBean> getLoanBookList() {
        return loanBookList;
    }

    public void setBookList(List<BookBean> loanBookList) {
        this.loanBookList = loanBookList;
    }
}
