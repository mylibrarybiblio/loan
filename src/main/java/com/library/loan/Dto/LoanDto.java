package com.library.loan.Dto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LoanDto {
    private int idLoan;
    private List<Integer> idBook = new ArrayList<>();
    private int idUser;
    private Date loanBeginDate = new Date();
    private Date loanEndDate = addDayToLoanDeginDate();

    public LoanDto() {
    }

    public LoanDto(List<Integer> idBook, int idUser) {
        this.idBook = idBook;
        this.idUser = idUser;
        //this.loanBeginDate = new Date();
        //this.loanEndDate = addDayToLoanDeginDate();
    }

    public int getIdLoan() {
        return idLoan;
    }

    public void setIdLoan(int idLoan) {
        this.idLoan = idLoan;
    }

    public List<Integer> getIdBook() {
        return idBook;
    }

    public void setIdBook(List<Integer> idBook) {
        this.idBook = idBook;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public Date getLoanBeginDate() {
        return loanBeginDate;
    }

    public void setLoanBeginDate(Date loanBeginDate) {
        this.loanBeginDate = loanBeginDate;
    }

    public Date getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(Date loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    private Date addDayToLoanDeginDate(){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 14);
        Date loanDeginDateADDTwoWeek = c.getTime();
        return loanDeginDateADDTwoWeek;
    }
}
