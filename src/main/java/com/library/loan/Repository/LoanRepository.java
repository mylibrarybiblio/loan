package com.library.loan.Repository;

import com.library.loan.Entity.Loan;
import com.library.loan.Entity.LoanStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer> {
    List<Loan> findByIdUserAndLoanStatus(int idUser, LoanStatus loanStatus);

    List<Loan> findByIdUser(int idUser);

    List<Loan> findByLoanBeginDateBefore(Date date);
}
