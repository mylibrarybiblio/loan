package com.library.loan.Controler;

import com.library.loan.Dto.LoanDto;
import com.library.loan.Dto.ResponseLoanDto;
import com.library.loan.Service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class LoanController {

    @Autowired
    private LoanService loanService;

    @PostMapping("/createNewLoan")
    public ResponseEntity<LoanDto>createNewLoan (@RequestBody LoanDto loanDto){
        loanService.createNewLoan(loanDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @GetMapping("/allLoan")
    public ResponseEntity<List<LoanDto>> listLoan(){
        List<LoanDto> loanDtoList = loanService.loanDtoList();
        return new ResponseEntity<>(loanDtoList, HttpStatus.FOUND);
    }

    @GetMapping("/searchLoanByEmail/{email}")
    public ResponseEntity<List<ResponseLoanDto>> searchLoanByEmail(@PathVariable String email){
        List<ResponseLoanDto> responseLoanDto = loanService.searchLoanByEmail(email);
        return new ResponseEntity<>(responseLoanDto,HttpStatus.FOUND);
    }
    @GetMapping("/loansBeforeDate/{date}")
    public ResponseEntity<List<ResponseLoanDto>> searchLoansBeforeThisDate(@PathVariable String date){
        List<ResponseLoanDto> loanDtoList = loanService.searchLoansBeforeThisDate(date);
        return new ResponseEntity<>(loanDtoList, HttpStatus.FOUND);
    }

    @PutMapping("/updateLoan")
    public ResponseEntity<LoanDto> updateLoan(@RequestBody LoanDto loanDto){
        LoanDto loanDto1 = loanService.updateLoan(loanDto);
        return new ResponseEntity<>(loanDto1, HttpStatus.OK);
    }

    @DeleteMapping("/deleteLoan/{idLoan}")
    public ResponseEntity<Void> deleteLoan(@PathVariable int idLoan){
        loanService.deleteLoan(idLoan);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
