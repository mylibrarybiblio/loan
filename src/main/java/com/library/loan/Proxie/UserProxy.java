package com.library.loan.Proxie;

import com.library.loan.Bean.UserBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "user", url = "localhost:8081/api")
public interface UserProxy {
    @GetMapping("/searchUser/{email}")
    List<UserBean> SearchUser(@PathVariable String email);
    @GetMapping("/user/{idUser}")
    UserBean oneUser(@PathVariable int idUser);

}
