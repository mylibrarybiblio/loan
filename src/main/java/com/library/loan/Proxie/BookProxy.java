package com.library.loan.Proxie;

import com.library.loan.Bean.BookBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "book", url = "localhost:8082/api")
public interface BookProxy {
    @GetMapping("/book/{idBook}")
    BookBean oneBook(@PathVariable int idBook);
}
