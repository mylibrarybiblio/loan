package com.library.loan;

import com.library.loan.Entity.Loan;
import com.library.loan.Entity.LoanStatus;
import com.library.loan.Repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@SpringBootApplication
@EnableFeignClients("com.library.loan")
public class LoanApplication implements CommandLineRunner {
	@Autowired
	private LoanRepository loanRepository;

	public static void main(String[] args) {
		SpringApplication.run(LoanApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Loan l1 = new Loan(List.of(1),1,f.parse("2021-05-08 11:55:02") ,f.parse("2021-05-22 11:55:02"),LoanStatus.CLOSE);
		Loan l2 = new Loan(List.of(1,2),2,f.parse("2021-05-10 11:55:02") ,f.parse("2021-05-24 11:55:02"),LoanStatus.OPEN);
		loanRepository.saveAll(List.of(l1,l2));
	}

}
