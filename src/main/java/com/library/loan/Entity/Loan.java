package com.library.loan.Entity;

import com.library.loan.Bean.UserBean;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
public class Loan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idLoan;
    @ElementCollection
    @CollectionTable(name = "loan_book", joinColumns = @JoinColumn(name = "id_loan"))
    @Column(name = "id_Book")
    private List<Integer> idBook = new ArrayList<>();
    private int idUser;
    private Date loanBeginDate;
    private Date loanEndDate;
    private LoanStatus loanStatus;

    public Loan(List<Integer> idBook, int idUser) {
        this.idBook = idBook;
        this.idUser = idUser;
    }

    public Loan(List<Integer> idBook, int idUser, Date loanBeginDate, Date loanEndDate, LoanStatus loanStatus) {
        this.idBook = idBook;
        this.idUser = idUser;
        this.loanBeginDate = loanBeginDate;
        this.loanEndDate = loanEndDate;
        this.loanStatus = loanStatus;
    }

    public Loan() {
    }

    public int getIdLoan() {
        return idLoan;
    }

    public void setIdLoan(int idLoan) {
        this.idLoan = idLoan;
    }

    public List<Integer> getIdBook() {
        return idBook;
    }

    public void setIdBook(List<Integer> idBook) {
        this.idBook = idBook;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public Date getLoanBeginDate() {
        return loanBeginDate;
    }

    public void setLoanBeginDate(Date loanBeginDate) {
        this.loanBeginDate = loanBeginDate;
    }

    public Date getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(Date loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public LoanStatus getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(LoanStatus loanStatus) {
        this.loanStatus = loanStatus;
    }

    private Date addDayToLoanDeginDate(){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 14);
        Date loanDeginDateADDTwoWeek = c.getTime();
        return loanDeginDateADDTwoWeek;
    }
}
