package com.library.loan.Bean;

public class CategoryBean {
    private int idCategory;
    private String label;

    public CategoryBean() {
    }

    public CategoryBean(String label) {
        this.label = label;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
